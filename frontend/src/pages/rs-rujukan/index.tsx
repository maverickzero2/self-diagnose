import React from 'react'
import { NextPage } from 'next'

import { Content, PageWrapper, Column } from 'components/layout'
import { UnstyledButton } from 'components/design-system/components/Common'
import { Stack, Text, Box } from 'components/design-system'
import { HospitalInfoCard } from 'modules/hospital'
import styled from '@emotion/styled'
import { themeGet } from '@styled-system/theme-get'
import { themeProps } from 'components/design-system/Theme'
import { fetch } from 'utils/api'
import { SearchIcon, CloseIcon } from 'components/icons'

const Section = Content.withComponent('section')
const SearchWrapper = styled('div')`
  display: inline-flex;
  width: 100%;
  height: 60px;
  padding: 20px;
  align-items: center;
  border-radius: 5px;
  background-color: ${themeGet('colors.accents02', themeProps.colors.accents02)};
`

const SearchInput = styled('input')`
  width: 100%;
  margin: 20px;
  color: ${themeGet('colors.foreground', themeProps.colors.foreground)};
  background-color: ${themeGet('colors.accents02', themeProps.colors.accents02)};
  border: none;
  outline: none;
`

const SearchButton = styled(UnstyledButton)`
  width: 100%;
  height: 60px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;

  &.disabled,
  &[disabled] {
    background-color: ${themeGet('colors.primary03', themeProps.colors.primary03)};
  }

  ${themeProps.mediaQueries.md} {
    display: none;
  }
`

const SmallSearchButton = styled(UnstyledButton)`
  width: 100px;
  height: 40px;
  margin-top: 0px;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  display: none;

  &.disabled,
  &[disabled] {
    background-color: ${themeGet('colors.primary03', themeProps.colors.primary03)};
  }

  ${themeProps.mediaQueries.md} {
    display: inline-flex;
  }
`

const ClearButton = styled(UnstyledButton)`
  display: block;
  line-height: inherit;

  ${themeProps.mediaQueries.md} {
    display: none;
  }
`

const MoreButton = styled(UnstyledButton)`
  width: 100%;
  height: 60px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;

  &.disabled,
  &[disabled] {
    background-color: ${themeGet('colors.primary03', themeProps.colors.primary03)};
  }
`

const TextWrapper = styled('div')`
  display: block;

  ${themeProps.mediaQueries.sm} {
    display: flex;
    justify-content: space-between;
  }
`

const Spinner = styled('div')`
  display: block;
  margin-left: auto;
  margin-right: auto;
  border: 5px solid transparent;
  border-top: 5px solid ${themeGet('colors.brandblue', themeProps.colors.brandblue)};
  border-left: 5px solid ${themeGet('colors.brandblue', themeProps.colors.brandblue)};
  border-right: 5px solid ${themeGet('colors.brandblue', themeProps.colors.brandblue)};
  border-radius: 50%;
  width: 50px;
  height: 50px;
  animation: spin 1s linear infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`

interface Hospital {
  id: number
  province: string
  city: string
  name: string
  address: string
  phone: string
  description: string
  lat: number
  long: number
}

const HospitalReferences: NextPage<{}> = () => {
  const Hospital_URL = `${process.env.BACKEND_API_BASE}/api/hospitals`
  const [isLoading, setLoading] = React.useState<boolean>(true)
  const [keyword, setKeyword] = React.useState<string>('')
  const [hospitals, setHospitals] = React.useState<Hospital[]>([])
  const [numberOfItems, setNumberOfItems] = React.useState<number>(10)
  const [textInfo, setTextInfo] = React.useState<string>('')

  React.useEffect(() => {
    fetchHospitalData()
  }, [])

  React.useEffect(() => {
    if (hospitals.length == 0) {
      if (keyword === '') {
        setTextInfo('Tidak ada data rumah sakit')
      } else {
        setTextInfo(`Data dengan kata kunci "${keyword}" tidak ditemukan. Coba lagi ya.`)
      }
    } else if (hospitals.length > 0) {
      if (keyword === '') {
        setTextInfo('Menampilkan semua rumah sakit')
      } else {
        setTextInfo(`Pencarian untuk kata kunci "${keyword}"`)
      }
    }
  }, [hospitals])

  const fetchHospitalData = (URL: string = Hospital_URL) => {
    setNumberOfItems(10)
    setLoading(true)
    fetch(URL)
      .then((result: Hospital[]) => {
        setHospitals(result)
        setLoading(false)
      })
      .catch(error => {
        console.log(error)
        setLoading(false)
      })
  }

  const getHospitalsByKeyword = () => {
    const URL = `${Hospital_URL}?query=${keyword}`
    fetchHospitalData(URL)
  }

  const getHospitalByLocation = () => {
    if (navigator.geolocation) {
      setKeyword('')
      navigator.geolocation.getCurrentPosition(position => {
        const latitude = position.coords.latitude
        const longitude = position.coords.longitude
        const URL = `${Hospital_URL}/nearby?lat=${latitude}&long=${longitude}`
        fetchHospitalData(URL)
      })
    }
  }

  const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setKeyword(e.currentTarget.value)
  }

  const handleSearchEnter = (e: React.KeyboardEvent<HTMLInputElement>) => {
    e.key === 'Enter' && getHospitalsByKeyword()
  }

  return (
    <PageWrapper title="Daftar Rumah Sakit">
      <Section noPadding={true}>
        <Column>
          <Stack>
            <Box padding="md">
              <SearchWrapper>
                <SearchIcon />
                <SearchInput
                  type="text"
                  placeholder="Nama kota, daerah, atau rumah sakit"
                  value={keyword}
                  onChange={handleSearchChange}
                  onKeyDown={handleSearchEnter}
                />
                <ClearButton disabled={isLoading} onClick={() => setKeyword('')} style={{ display: keyword ? 'block' : 'none' }}>
                  <CloseIcon width={20} />
                </ClearButton>

                <SmallSearchButton type="button" mt="sm" backgroundColor="brandblue" disabled={isLoading} onClick={getHospitalsByKeyword}>
                  Cari RS
                </SmallSearchButton>
              </SearchWrapper>
              <Text as="p" mt="sm" style={{ textAlign: 'right' }}>
                <UnstyledButton disabled={isLoading} onClick={getHospitalByLocation}>
                  <u>Gunakan Lokasi Terdekat</u>
                </UnstyledButton>
              </Text>
              <SearchButton type="button" mt="sm" backgroundColor="brandblue" disabled={isLoading} onClick={getHospitalsByKeyword}>
                Cari Rumah Sakit
              </SearchButton>
            </Box>
            <Box padding="md">
              {isLoading ? (
                <Box justifyContent="center" textAlign="center">
                  <Spinner />
                  <Text as="p" fontWeight="bold" variant={500} mt="md">
                    Sedang mencari rumah sakit
                  </Text>
                  <Text as="p" color="accents05" mt="sm">
                    Tunggu sebentar
                  </Text>
                </Box>
              ) : (
                <>
                  <TextWrapper>
                    <Text as="p" textAlign={hospitals.length > 0 ? 'left' : 'center'} variant={600}>
                      {textInfo}
                    </Text>
                    {hospitals.length > 0 && (
                      <Text as="p" color="accents05" variant={200} mt="sm">
                        Ada {hospitals.length} total data
                      </Text>
                    )}
                  </TextWrapper>

                  {hospitals.length > 0 && (
                    <>
                      {hospitals.length >= 0 &&
                        hospitals.slice(0, numberOfItems).map((hospital: Hospital) => {
                          return (
                            <HospitalInfoCard
                              key={`hospital-card--${hospital.id}`}
                              id={hospital.id}
                              name={hospital.name}
                              address={`${hospital.address}, ${hospital.city}, ${hospital.province}`}
                              phoneNumber={hospital.phone}
                            />
                          )
                        })}
                      {hospitals.length - numberOfItems > 0 && (
                        <MoreButton type="button" onClick={() => setNumberOfItems(numberOfItems + 10)} backgroundColor="brandblue" mt="sm">
                          Muat lebih banyak
                        </MoreButton>
                      )}
                    </>
                  )}
                </>
              )}
            </Box>
          </Stack>
        </Column>
      </Section>
    </PageWrapper>
  )
}

export default HospitalReferences
