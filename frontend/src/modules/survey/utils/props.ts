export interface QuestionProps {
  key?: string
  nextState?: number
  questionId?: number
  question?: string
  answer: string
}
export interface SurveyPageProps {
  isDarkMode: boolean
  onAnswer: (question: QuestionProps) => void
}
export interface ResultProps {
  age?: string
  gender?: string
  status?: string
  browserInfo?: string
  location?: string
  questions: Array<QuestionProps>
}
