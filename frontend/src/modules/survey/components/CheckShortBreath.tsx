import * as React from 'react'

import { Box } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import ButtonGroups from './ButtonGroups'
import SurveyQuestion from './SurveyQuestion'

let configs = [
  { key: '7a', label: 'Ya', nextState: 99, answer: 'true' },
  { key: '7b', label: 'Tidak', nextState: 8, answer: 'false' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 7,
  question:
    'Dalam satu jam terakhir, apakah Anda merasa lebih sesak atau sulit bernapas dari biasanya?'
}))
const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <SurveyQuestion>
          Dalam<b> satu jam terakhir</b>, apakah Anda merasa<b> lebih sesak atau sulit bernapas </b>
          dari biasanya?
        </SurveyQuestion>
      </Box>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
