import * as React from 'react'

import { Box } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import ButtonGroups from './ButtonGroups'
import SurveyQuestion from './SurveyQuestion'

let configs = [
  { key: '4a', label: 'Ya', nextState: 5, answer: 'true' },
  { key: '4b', label: 'Tidak', nextState: 5, answer: 'false' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 4,
  question: 'Apakah Anda batuk tidak berdahak terus-menerus?'
}))
const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <SurveyQuestion>
          Apakah Anda <b> batuk tidak berdahak </b>terus-menerus?
        </SurveyQuestion>
      </Box>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
