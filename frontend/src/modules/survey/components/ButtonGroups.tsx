import * as React from 'react'

import { UnstyledButton, Box } from 'components/design-system'
// eslint-disable-next-line import/no-extraneous-dependencies
import styled from '@emotion/styled-base'
import { QuestionProps } from 'modules/survey/utils/props'

export interface Config {
  key: string
  label: string
  nextState: number
  answer: string
}
export interface ButtonGroupsProps {
  title?: string
  configs: Array<Config>
  onAnswer: (question: QuestionProps) => void
}

const Button = styled(UnstyledButton)`
  height: 60px;
  padding: 20px;
  text-transform: uppercase;
  font-weight: 600;
  font-family: IBM Plex Sans;
  background: #3389fe;
  border-radius: 4px;
  margin-right: 8px;
  text-align: center;
  width: 288px;
`

const ButtonGroups: React.FC<ButtonGroupsProps> = ({ title, onAnswer, configs }) => {
  return (
    <Box display="flex" alignItems="center" justifyContent="center">
      {title && <div>{title}</div>}
      {configs.map(config => {
        return (
          <Button
            key={config.key}
            onClick={() => {
              onAnswer(config)
            }}
          >
            {config.label}
          </Button>
        )
      })}
    </Box>
  )
}
export default ButtonGroups
