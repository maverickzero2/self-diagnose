import * as React from 'react'

import { Box } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import ButtonGroups from './ButtonGroups'
import SurveyQuestion from './SurveyQuestion'

let configs = [
  { key: '6a', label: 'Ya', nextState: 99, answer: 'true' },
  { key: '6b', label: 'Tidak', nextState: 7, answer: 'false' },
  { key: 'c', label: 'Tidak Tahu', nextState: 7, answer: 'maybe' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 6,
  question: 'Apakah Anda mengalami sesak atau sulit bernapas?'
}))

const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <SurveyQuestion>
          Apakah Anda mengalami<b> sesak atau sulit bernapas?</b>
        </SurveyQuestion>
      </Box>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
