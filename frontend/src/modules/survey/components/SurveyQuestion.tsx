import { Paragraph } from 'components/design-system'
// eslint-disable-next-line import/no-extraneous-dependencies
import styled from '@emotion/styled-base'

const SurveyQuestion = styled(Paragraph)`
  font-size: 24px;
  height: 60vh;
`
export default SurveyQuestion
