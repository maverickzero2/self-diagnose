import * as React from 'react'

import { Heading, Paragraph, Box } from 'components/design-system'

import { SurveyPageProps } from 'modules/survey/utils/props'
import SickDarkImage from '../../../assets/images/sick-dark.svg'
import SickLightImage from '../../../assets/images/sick-light.svg'

import ButtonGroups from './ButtonGroups'

const renderSickImage = (isDarkMode: boolean) => {
  if (isDarkMode) {
    return <SickDarkImage />
  }
  return <SickLightImage />
}
const configs = [{ key: '0', label: 'Mulai', nextState: 1, answer: '' }]

const SurveyHome: React.FC<SurveyPageProps> = ({ isDarkMode, onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        {renderSickImage(isDarkMode)}
      </Box>
      <Heading variant={900} mb="lg" as="h1">
        Halo! <br />
        Selamat datang di layanan periksa mandiri COVID-19
      </Heading>
      <Paragraph variant={500}>
        Layanan ini dibuat secara sukarela oleh situs KawalCOVID19.id untuk membantu masyarakat
        memeriksa secara mandiri kondisi tubuhnya sebelum menuju ke rumah sakit agar dapat
        mengurangi jumlah pasien yang harus ditangani.
      </Paragraph>
      <br />
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
