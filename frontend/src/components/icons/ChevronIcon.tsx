import React from 'react'
import { themeGet } from '@styled-system/theme-get'
import styled from '@emotion/styled-base'
import { themeProps } from 'components/design-system'

const Icon = styled('svg')`
  fill: ${themeGet('colors.foreground', themeProps.colors.foreground)};
`

const ChevronIcon: React.FC<React.SVGProps<SVGSVGElement>> = ({ width = 6, height = 11, fill, ...rest }) => {
  return (
    <Icon width={width} height={height} viewBox="0 0 6 11" fill="none" {...rest}>
      <path
        d="M.563.414L.093.86C0 .977 0 1.164.094 1.258L4.337 5.5.094 9.766c-.094.093-.094.28 0 .398l.469.445a.27.27 0 00.398 0l4.922-4.898a.317.317 0 000-.399L.96.415a.27.27 0 00-.398 0z"
        fill={fill}
      />
    </Icon>
  )
}

export default ChevronIcon
