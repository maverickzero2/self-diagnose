import FuzzySearch from "fuzzy-search";
import Tabletop from "tabletop";
import { EntityRepository, Repository } from "typeorm";

import { Hospital } from "../entities/Hospital";
import { Cache } from "../utils/Cache";
import { getDistanceBetweenPoints } from "../utils/Geo";
import { logger } from "../utils/Logger";

//region data-source
const publicSpreadsheetUrl =
    "https://docs.google.com/spreadsheets/d/1GssYbmC1kVrZj0wNAtPa0yIsKcacztx23FhABn1azic/edit?usp=sharing";
const selectedSheet = "FixedData";
//endregion

//region cache
//cache TTL config
const ALL_HOSPITALS_CACHE_TTL: number = 6 * 60 * 60; //6 hours
const SPECIFIC_QUERY_CACHE_TTL: number = 5 * 60; //5 minutes

//cache instance
const cache = new Cache(ALL_HOSPITALS_CACHE_TTL);
const queryCache = new Cache(SPECIFIC_QUERY_CACHE_TTL);
//endregion

@EntityRepository(Hospital)
export class HospitalRepository extends Repository<Hospital> {
    public async getHospitals(): Promise<Array<Hospital>> {
        const key: string = "all";

        if (cache.has(key)) {
            logger.info(`cache with key ${key} HIT`);
            return cache.get(key);
        }

        logger.info(`cache with key ${key} MISS`);
        const fetchResult = await Tabletop.init({
            key: publicSpreadsheetUrl,
            parseNumbers: true,
            orderby: "id",
        });

        const { elements } = fetchResult[selectedSheet];
        cache.set(key, elements);
        return elements;
    }

    public async getHospitalsByQuery(query: string): Promise<Array<Hospital>> {
        return this._filterHospitalByPropertyKey("query", query);
    }

    public async getHospitalsByProvince(province: string): Promise<Array<Hospital>> {
        return this._filterHospitalByPropertyKey("province", province);
    }

    public async getHospitalsByCity(city: string): Promise<Array<Hospital>> {
        return this._filterHospitalByPropertyKey("city", city);
    }

    public async getHospitalsByName(name: string): Promise<Array<Hospital>> {
        return this._filterHospitalByPropertyKey("name", name);
    }

    public async getHospitalByID(id: string): Promise<Array<Hospital>> {
        return this._filterHospitalByPropertyKey("id", id, true);
    }

    public async getHospitalsNearby(lat: number, long: number): Promise<Array<Hospital>> {
        const hospitals = await this.getHospitals();

        //FIXME: I don't think that calculate this on backend is a good thing.
        //Eventually we should move these hospital list to our own data-store
        //Intentionally not put the calculation to cache since lat,long combination is way to big
        return hospitals
            .map(hospital => {
                //calculate this using some black magic math formula
                const distance = getDistanceBetweenPoints(lat, long, hospital.lat, hospital.long);
                hospital.distance = distance;
                return hospital;
            })
            .sort((foo: Hospital, bar: Hospital) => {
                //sort by distance ascending
                return foo.distance >= bar.distance ? 1 : -1;
            })
            .slice(0, 10); //get top 10 m̶o̶d̶e̶l̶
    }

    private async _filterHospitalByPropertyKey(
        propertyKey: string,
        query: string,
        exactSearch: boolean = false,
    ): Promise<Array<Hospital>> {
        //if the given propertyKey is `query` it will fuzzy search name, city, and province
        //if it's not, then it will search based on the propertyKey given
        const searchPropertyKey = propertyKey === "query" ? ["name", "city", "province"] : [`${propertyKey}`];
        let result = undefined;

        const key: string = `${propertyKey}:${query}`;

        //check if there is cache key exists
        if (cache.has(key)) {
            logger.info(`cache with key ${key} HIT`);
            return cache.get(key);
        }

        logger.info(`cache with key ${key} MISS`);
        //fetch from service instead of create new Tabletop instance to utilize cache
        const hospitals = await this.getHospitals();

        // Check if it needs exact key filter or not
        if (exactSearch) {
            result = hospitals.filter(hospital => hospital[searchPropertyKey[0]].toString() === query);
        } else {
            //filter the hospitals by its property name
            const searcher = new FuzzySearch(hospitals, searchPropertyKey, {
                caseSensitive: false,
                sort: true,
            });
            result = searcher.search(query);
        }

        //set to cache, put 5 minutes cache for specific queries so it would be flushed and not consume RAM
        queryCache.set(key, result);

        return result;
    }
}
