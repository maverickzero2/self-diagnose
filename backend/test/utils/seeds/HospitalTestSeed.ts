export const HospitalSeed = [
    {
        id: 1,
        name: "RSU Dr. Zainoel Abidin Banda Aceh",
        province: "Aceh",
        city: "Lhokseumawe",
        address: "Jl. Tgk. Daud Beureueh No.108 banda Aceh Telp: 0651 - 22077, 28148",
        phone: "0651-22077",
        description: "Tidak ada di data kemenkes",
        lat: -0.943174,
        long: 100.366944,
    },
    {
        id: 103,
        name: "RSUD dr. Agoesdjam Ketapang",
        province: "Kalimantan Barat",
        city: "Ketapang",
        address: "Jl. DI Panjaitan No.51, Sampit, Delta Pawan, Kabupaten Ketapang, Kalimantan Barat 78811",
        phone: "(0534) 3037239",
        description: "Tambahan dari data kemenkes",
        lat: -1.832675,
        long: 109.967477,
    },
];
