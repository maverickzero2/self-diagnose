import { agent, Response } from "supertest";
import { Connection } from "typeorm";

import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import app from "../utils/testApp";

let db: Connection;

beforeAll(async () => {
    db = await createMemoryDatabase();
});

afterAll(async done => {
    await db.close();
    done();
});

describe("POST /api/surveys", () => {
    let payload =  {
        "age": 17,
        "browserInfo": "MozillaFirefox/ChromeOSX10",
        "location": "Serpong",
        "status": "warning",
        "questions": [
            {
                "questionId": 2,
                "question": "Apakah anda batuk?",
                "answer": true
            },
            {
                "questionId": 1,
                "question": "Apakah anda sesak nafas?",
                "answer": true
            }
        ]
    };

    it("201: Successfully store survey", done => {
        agent(app)
            .post("/api/surveys")
            .send(payload)
            .expect(201)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                expect(body.id).not.toBe(null);
                expect(body.age).toBe(payload.age);
                expect(body.browserInfo).toBe(payload.browserInfo);
                expect(body.location).toBe(payload.location);
                expect(body.status).toBe(payload.status);
                expect(body.questions).toBe(JSON.stringify(payload.questions));
                done();
            });
    });
});
